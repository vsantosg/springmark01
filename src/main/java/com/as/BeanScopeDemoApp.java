package com.as;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class BeanScopeDemoApp {

	public static void main(String[] args) {

		// carga el archivo de configuracion
		ClassPathXmlApplicationContext context =new
				ClassPathXmlApplicationContext("beanScope-applicationContext.xml");
				
		// bean spring container
		Catedratico elCatedratico = context.getBean("miCatedratico", Catedratico.class);

		Catedratico alphaCoach = context.getBean("miCatedratico", Catedratico.class);
		
		// verifican si son el mismo
		boolean result = (elCatedratico == alphaCoach);
		
		// despliega en consola resultados
		System.out.println("\nespacio del mismo objeto: " + result);
		
		System.out.println("\nEspacio de memoria de theCoach: " + elCatedratico);

		System.out.println("\nEspacio de memoria de alphaCoach: " + alphaCoach + "\n");
	
		// cierra conexion
		context.close();
	}

}








