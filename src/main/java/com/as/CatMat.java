package com.as;

public class CatMat implements Catedratico {

    // ddefinir como privado el metodo de dependencia
    private BuenServicio fortuneService;

    // define el construcctor de la inject
	public CatMat(BuenServicio theFortuneService) {
        fortuneService = theFortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "las clases seran diarias ";
    }

    @Override
    public String getDailyFortune() {

        return fortuneService.getFortune();
    }
}
