package com.as;

public class CatIngles implements Catedratico {

    private BuenServicio fortuneService;

    // agrega campos
    private String email;
    private String team;


    // constructor
    public CatIngles() {
        System.out.println("CricketCoach: inside no-arg constructor");
    }

    public String getEmailAddress() {
        return email;
    }

    public void setEmailAddress(String emailAddress) {
        System.out.println("Catedratico Ingles: inside setter method - setEmailAddress");
        this.email = emailAddress;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        System.out.println("Cateratico Ingles: inside setter method - setTeam");
        this.team = team;
    }

    // setter
    public void setFortuneService(BuenServicio fortuneService) {
        System.out.println("catedratico :  setFortuneService");
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "Practica durante 30 minutos conversacion";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

}
