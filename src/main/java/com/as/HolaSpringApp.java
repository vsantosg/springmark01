package com.as;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HolaSpringApp {

    public static void main(String[] args) {

        // carga el archivo de confirguracion de spring
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("src/applicationContext.xml");

        // bean spring container
        Catedratico elCatedratico = context.getBean("miCatedratico", Catedratico.class);

        // llama a los metodos del spring
        System.out.println(elCatedratico.getDailyWorkout());

        // llama al metodo
        System.out.println(elCatedratico.getDailyFortune());

        // cierra el contxto
        context.close();
    }

}


