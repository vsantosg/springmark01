package com.as;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SetterDemoApp {

	public static void main(String[] args) {

		// archivo de configuracion de spring
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("src/applicationContext.xml");
		
		// bean del spring container
		CatIngles elCatedratico = context.getBean("profe de ingles", CatIngles.class);
		
		// llama a los metodos.
		System.out.println(elCatedratico.getDailyWorkout());
		
		System.out.println(elCatedratico.getDailyFortune());
		
		// metodos
		System.out.println(elCatedratico.getEmailAddress());
		
		System.out.println(elCatedratico.getTeam());
				
		// cierra el contexto
		context.close();
	}

}






